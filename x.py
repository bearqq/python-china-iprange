#!/usr/bin/env python
# coding=utf-8
# code by 92ez.com
# last modify time 2016-08 by bearqq

#import requests
import sys

def getLastestIpData():
    #url = 'http://ipblock.chacuo.net/down/t_txt=c_CN'
    #print '[Note] Try to get IP data from '+ url +' ....'
    try:
        f=open('t_txt=c_CN.htm','r')
        content=f.read()
        dataContent = content.replace('\t',' ').replace('\r\n','\n')
        print '[Note] Get IP data success! Data length is '+ str(len(dataContent)) +'.'
        logFile = open(sys.path[0] + '/cnip.log','w')
        try:
            logFile.write(dataContent)
            logFile.close()
            print '[Note] Write data into log file success! Path : '+sys.path[0] + '/cnip.log'
        except Exception,e:
            print e
            print '[Note] Program exit...'
            sys.exit()
    except Exception,e:
        print e
        print '[Note] Program exit...'
        sys.exit()
    
def decodeIpdata():
    getLastestIpData()
    resultIpArray = []
    totalIp = 0
    #decode data
    logFile = open(sys.path[0] + '/cnip.log')
    
    for line in logFile:
        resultIpArray.append(line.split(' ')[0]+'-'+line.split(' ')[1])
        totalIp = totalIp + int(line.split(' ')[3])

    logFile.close()

    logFile = open(sys.path[0] + '/cnip.log','w')
    logFile.write(str(resultIpArray))
    logFile.close()

    print '[Note] Total '+str(len(resultIpArray))+' IP range.'
    print '[Note] Total '+str(totalIp)+' IP.'

if __name__ == "__main__":
    decodeIpdata()
    print '[Note] Finish!'